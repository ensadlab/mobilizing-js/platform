import 'source-map-support/register';
import { Server as SoundworksServer } from '@soundworks/core/server';

import getConfig from './utils/getConfig.js';
import pluginPlatform from '../shared/pluginPlatform.js';

// import services
import { ClientExperience } from './ClientExperience.js';

import path from 'path';
import serveStatic from 'serve-static';
import compile from 'template-literal';

export class Server extends SoundworksServer {
  constructor({
    environment = process.env.ENV || 'default',
    soundworksServerOptions,
    features = [],
  } = {}) {
    // composition
    // this.server = new SoundworksServer(soundworksServerOptions);

    // inheritance
    super(soundworksServerOptions);

    this.environment = environment;
    this.features = features;
    this.configuration = getConfig(this.environment);

    // html template and static files (in most case, this should not be modified)
    this.templateEngine = { compile };
    this.templateDirectory = path.join('.build', 'server', 'templates');
    this.router.use(serveStatic('public'));
    this.router.use('build', serveStatic(path.join('.build', 'public')));
    this.router.use('vendors', serveStatic(path.join('.vendors', 'public')));

    // // expose part of soundworks API
    // this.router = this.server.router;
    // this.pluginManager = this.server.pluginManager;
    // this.stateManager = this.server.stateManager;


    console.log(`
--------------------------------------------------------
- launching "${this.configuration.app.name}" in "${this.environment}" environment
- [pid: ${process.pid}]
--------------------------------------------------------
`);
  }

  async init({
    configuration = this.configuration,
  } = {}) {
    await super.init(configuration, (clientType, config, httpRequest) => {
      return {
        clientType,
        app: {
          name: config.app.name,
          author: config.app.author,
        },
        env: {
          type: config.env.type,
          websockets: config.env.websockets,
          assetsDomain: config.env.assetsDomain,
        }
      };
    });

    // definitions for features of platform plugin
    const pluginPlatformRequired
          = pluginPlatform.isRequiredForFeatures(this.features);

    if(pluginPlatformRequired) {
      const {default: pluginPlatformFactory}
            = await import('@soundworks/plugin-platform/server/index.js');

      this.pluginManager.register('platform', pluginPlatformFactory);

    }

    this.clientTypes = Object.keys(this.configuration.app.clients);
    // use a single server-side experience for all client types
    // (it is possible to differentiate them for advanced uses)
    this.clientExperience = new ClientExperience({
      server: this,
      clientTypes: this.clientTypes,
      features: this.features,
    });


  }

  async start() {
    await super.start();
    await this.clientExperience.start();
  };

}

export default Server;

