import 'core-js/stable';
import 'regenerator-runtime/runtime';
import soundworks from '@soundworks/core/client';
const SoundworksClient = soundworks.Client;
import pluginPlatform from '../shared/pluginPlatform.js';
import initQoS from '@soundworks/template-helpers/client/init-qos.js';

import { ClientExperience } from './ClientExperience.js';
import renderInitializationScreens from '@soundworks/template-helpers/client/render-initialization-screens.js';

const e = {};

export class Client extends SoundworksClient {
  constructor({
    soundworksClientOptions,
    features = [],
    container =  document.querySelector('body'),
    clientsCount = 1,
    autoReload = {
      onSocketClose: true,
      onVisibilityChange: true,
    },
  } = {}) {
    // composition
    // this.client = new SoundworksClient(soundworksClientOptions);

    // inheritance
    super(soundworksClientOptions);
    this.featuresRequest = features;

    this.container = container;

    this.configuration = window.soundworksConfig;
    this.experience = new ClientExperience(this, {
      config: this.configuration,
      container: this.container,
    });

    this.autoReload = autoReload;

    renderInitializationScreens(this, this.configuration, this.container);
  }

  async init({
    configuration = this.configuration,
  } = {}) {
    await super.init(configuration);

    this.features = [];
    // definitions for features of platform plugin
    const pluginPlatformRequired
          = pluginPlatform.isRequiredForFeatures(this.featuresRequest);

    if(pluginPlatformRequired) {
      const {default: pluginPlatformFactory}
            = await import('@soundworks/plugin-platform/client/index.js');

      if(this.featuresRequest.includes('devicemotion') ) {
        const {default: devicemotion}
              = await import('@ircam/devicemotion');

        this.features.devicemotion = devicemotion;
        pluginPlatformFactory.addFeatureDefinition({
          id: 'devicemotion',
          initialize: async () => {
            const result = await devicemotion.requestPermission();
            const deviceMotionEnabled = (result === 'granted' ? true : false);
            if(!deviceMotionEnabled) {
              console.warn('No device motion access');
              this.features.devicemotion = null;
            }
            // always return `true` as we don't want to block the application at
            // this point, this must be the application responsibility to display
            // an error message if the client requires `deviceMotion`
            return true;
          },
        });

      } // devicemotion

      this.pluginManager.register('platform', pluginPlatformFactory, {
        features: pluginPlatform.providesFeatures(this.features),
      }, []);


    } // pluginPlatformRequired

    // restart client when connection is lost (suspend activity, no network, etc.)
    initQoS(this, {
      socketClosed: this.autoReload.onSocketClose,
      visibilityChange: this.autoReload.onVisibilityChange,
    });
  }

  async start(options) {
    document.body.classList.remove('loading');
    await super.start(options);

    this.experience.start();
  }

}
Object.assign(e, {Client});

export default e;

