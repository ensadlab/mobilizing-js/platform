import 'regenerator-runtime/runtime';
import { AbstractExperience } from '@soundworks/core/client';
import pluginPlatform from '../shared/pluginPlatform.js';

export class ClientExperience extends AbstractExperience {
  constructor(client, {
    features = [],
  } = {}) {
    super(client);

    this.features = features;

    const pluginsRequired = [];
    if(pluginPlatform.isRequiredForFeatures(features) ) {
      pluginsRequired.push('platform');
    }

    // require plugins if needed
    this.plugins = {};
    pluginsRequired.forEach( (pluginName) => {
      this.plugins[pluginName] = this.require(pluginName);
    });

  }

  start() {
    super.start();
  }

}

export default ClientExperience;
