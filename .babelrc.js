module.exports = {
  presets: [
    [
      "@babel/preset-env", {
        useBuiltIns: 'usage',
        corejs: 3,
      },
    ],
  ],
  plugins: [
    "@babel/plugin-transform-modules-commonjs",
    "@babel/plugin-proposal-class-properties",
    '@babel/plugin-proposal-export-default-from',
  ],
}
